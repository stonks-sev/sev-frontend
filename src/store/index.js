import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    accessToken: null,
    favStreamer: null,
  },
  mutations: {
    setAccessToken (state, accessToken) {
      state.accessToken = accessToken;
    },
    setFavStreamer (state, favStreamer) {
      state.favStreamer = favStreamer;
    }
  },
  actions: {
  },
  modules: {
  }
})
